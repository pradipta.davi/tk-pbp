from django.contrib import admin
from .models import Profile, Consultation, Booking

# admin.site.register(Profile)
admin.site.register(Consultation)
admin.site.register(Booking)
