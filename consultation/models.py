from django import forms
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from authentication.models import Profile

DAY_CHOICES = (
    ("Senin", "Senin"),
    ("Selasa","Selasa"),
    ("Rabu", "Rabu"),
    ("Kamis", "Kamis"),
    ("Jumat", "Jumat")
)


class Consultation(models.Model):
    dokter = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    nama_dokter = models.CharField(max_length=50, default=dokter, null=True)
    hari_konsultasi = models.CharField(max_length=6, choices=DAY_CHOICES, default="Senin")
    waktu_konsultasi_start = models.TimeField(null=True)
    waktu_konsultasi_end = models.TimeField(null=True)


class Booking(models.Model):
    dokter = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    nama_dokter = models.CharField(max_length=50, default=dokter.name, null=True)
    konsultasi = models.TimeField(null=True)
    hari_konsultasi = models.CharField(max_length=6, choices=DAY_CHOICES, null=True)
    nama_pasien = models.CharField(max_length=100)
    nomor_handphone = models.CharField(max_length=15)
    email = models.EmailField(max_length=100)
