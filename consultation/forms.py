from django import forms
from django.forms import fields, Select
from .models import Profile, Consultation, Booking

DAY_CHOICES = (
        ("Senin", "Senin"),
        ("Selasa", "Selasa"),
        ("Rabu", "Rabu"),
        ("Kamis", "Kamis"),
        ("Jumat", "Jumat")
    )


class ConsultationForm(forms.Form):
    dokter = forms.ModelChoiceField(queryset=Profile.objects.filter(role='doctor'))
    konsultasi = forms.TimeField()
    hari_konsultasi = forms.CharField(max_length=6, widget=forms.Select(choices=DAY_CHOICES))
    nama_pasien = forms.CharField(max_length=100)
    nomor_handphone = forms.CharField(max_length=15)
    email = forms.EmailField(max_length=100)

    class Meta:
        fields = ("konsultasi", "nama_pasien", "hari_konsultasi", "nomor_handphone", "email")

    def save (self):
        data = self.cleaned_data
        tmp=data['dokter']
        print(data)
        booking = Booking(dokter=data['dokter'], nama_dokter=tmp.name, konsultasi=data['konsultasi'], hari_konsultasi=data['hari_konsultasi'],
                          nama_pasien=data['nama_pasien'], nomor_handphone=data['nomor_handphone'], email=data['email'])
        booking.save()


# class AddConsultationForm(forms.Form):
#     hari_konsultasi = forms.CharField(max_length=6, widget=forms.Select(choices=DAY_CHOICES))
#     waktu_konsultasi_start = forms.TimeField()
#     waktu_konsultasi_end = forms.TimeField()
#
#     class Meta:
#         fields = ("hari_konsultasi", "waktu_konsultasi_start", "waktu_konsultasi_end")
#
#     def save(self):
#         data = self.cleaned_data
#         consultation = Consultation(dokter=Profile, hari_konsultasi=data['hari_konsultasi'],
#                                     waktu_konsultasi_start=data['waktu_konsultasi_start'],
#                                     waktu_konsultasi_end=data['waktu_konsultasi_end'])
#         consultation.save()