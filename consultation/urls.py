from django.urls import path
from .views import consultation_doctor, consultation_patient, consultation_form, delete_booking, search_consultation, flutter_consultation_patient, flutter_consultation_doctor, flutter_consultation_form, flutter_delete_booking, flutter_get_profile, flutter_cek_role
from django.views.decorators.csrf import csrf_exempt

app_name = 'consultation'

urlpatterns = [
    path('', consultation_patient, name='consultation-patient'),
    path('doctor/', consultation_doctor, name='consultation-doctor'),
    path('form/', consultation_form, name='consultation-form'),
    path('delete-booking/<int:pk>', delete_booking, name='delete-booking'),
    path('search-consultation/', csrf_exempt(search_consultation), name='search_consultation'),
    path('flutter-consultation-patient', flutter_consultation_patient, name='flutter-consultation-patient'),
    path('flutter-consultation-doctor', flutter_consultation_doctor, name='flutter-consultation-doctor'),
    path('flutter-consultation-form', flutter_consultation_form, name='flutter-consultation-form'),
    path('flutter-delete-booking', flutter_delete_booking, name='flutter-delete-booking'),
    path('flutter-get-profile', flutter_get_profile, name='flutter-get-profile'),
    path('flutter-cek-role', flutter_cek_role, name='flutter-cek-role'),
]