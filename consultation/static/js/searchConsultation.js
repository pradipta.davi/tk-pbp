// const searchField=document.querySelector("#searchField");
const searchField=document.getElementById('searchField')
const consultationSenin=document.getElementById('consultation_senin')
const consultationSelasa=document.getElementById('consultation_selasa')
const consultationRabu=document.getElementById('consultation_rabu')
const consultationKamis=document.getElementById('consultation_kamis')
const consultationJumat=document.getElementById('consultation_jumat')

searchField.addEventListener('keyup', (e) => {

    const searchValue = e.target.value;

    if (searchValue.trim().length > 0) {
        console.log('searchValue', searchValue)

        fetch("search-consultation/", {
            body: JSON.stringify({searchText: searchValue}),
            method: "POST",
        })
            .then((res) => res.json())
            .then((data) => {
                consultationSenin.innerHTML = ""
                consultationSelasa.innerHTML = ""
                consultationRabu.innerHTML = ""
                consultationKamis.innerHTML = ""
                consultationJumat.innerHTML = ""

                console.log("data", data)

                data.forEach((element) => {
                    // console.log(element.hari_konsultasi)
                    if (element.hari_konsultasi === 'Senin') {
                        consultationSenin.innerHTML += `
                        <li class="list-group-item" style="background-color: #808080"">
                            <div class="d-flex justify-content-start">
                                <div class="col-sm" style="color: #FFFFFF"> ${element.waktu_konsultasi_start} - ${element.waktu_konsultasi_end} </div>
                                <div class="col-sm" style="color: #FFFFFF"> ${element.dokter_id} </div>
                            </div>
                        </li>
                        `
                    } else if (element.hari_konsultasi === 'Selasa') {
                        consultationSelasa.innerHTML += `
                        <li class="list-group-item" style="background-color: #808080"">
                            <div class="d-flex justify-content-start">
                                <div class="col-sm" style="color: #FFFFFF"> ${element.waktu_konsultasi_start} - ${element.waktu_konsultasi_end} </div>
                                <div class="col-sm" style="color: #FFFFFF"> ${element.dokter_id} </div>
                            </div>
                        </li>
                        `
                    } else if (element.hari_konsultasi === 'Rabu') {
                        consultationRabu.innerHTML += `
                        <li class="list-group-item" style="background-color: #808080" id="consultation_rabu">
                            <div class="d-flex justify-content-start">
                                <div class="col-sm" style="color: #FFFFFF"> ${element.waktu_konsultasi_start} - ${element.waktu_konsultasi_end} </div>
                                <div class="col-sm" style="color: #FFFFFF"> ${element.dokter_id} </div>
                            </div>
                        </li>
                        `
                    } else if (element.hari_konsultasi === 'Kamis') {
                        consultationKamis.innerHTML += `
                        <li class="list-group-item" style="background-color: #808080" id="consultation_kamis">
                            <div class="d-flex justify-content-start">
                                <div class="col-sm" style="color: #FFFFFF"> ${element.waktu_konsultasi_start} - ${element.waktu_konsultasi_end} </div>
                                <div class="col-sm" style="color: #FFFFFF"> ${element.dokter_id} </div>
                            </div>
                        </li>
                        `
                    } else if (element.hari_konsultasi === 'Jumat') {
                        consultationJumat.innerHTML += `
                        <li class="list-group-item" style="background-color: #808080" id="consultation_jumat">
                            <div class="d-flex justify-content-start">
                                <div class="col-sm" style="color: #FFFFFF"> ${element.waktu_konsultasi_start} - ${element.waktu_konsultasi_end} </div>
                                <div class="col-sm" style="color: #FFFFFF"> ${element.dokter_id} </div>
                            </div>
                        </li>
                        `
                    }
                })
            })
    }
});