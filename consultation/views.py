from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from .models import Profile, Consultation, Booking
from .forms import ConsultationForm
from django.http.response import JsonResponse, HttpResponse
from django.core import serializers
from collections import defaultdict
from django.views.decorators.csrf import csrf_exempt
import json


@login_required(login_url='/accounts/login')
def consultation_patient(request):
    consultation_senin = Consultation.objects.filter(hari_konsultasi='Senin')
    consultation_selasa = Consultation.objects.filter(hari_konsultasi='Selasa')
    consultation_rabu = Consultation.objects.filter(hari_konsultasi='Rabu')
    consultation_kamis = Consultation.objects.filter(hari_konsultasi='Kamis')
    consultation_jumat = Consultation.objects.filter(hari_konsultasi='Jumat')
    response = {'consultation_senin': consultation_senin, 'consultation_selasa': consultation_selasa,
                'consultation_rabu': consultation_rabu, 'consultation_kamis': consultation_kamis,
                'consultation_jumat': consultation_jumat}
    return render(request, 'consultation_patient.html', response)


@login_required(login_url='/accounts/login')
def consultation_doctor(request):
    doctor = Profile.objects.filter(user=request.user)[0]
    if doctor.role == 'doctor':
        booking_senin = Booking.objects.filter(dokter=doctor, hari_konsultasi='Senin')
        booking_selasa = Booking.objects.filter(dokter=doctor, hari_konsultasi='Selasa')
        booking_rabu = Booking.objects.filter(dokter=doctor, hari_konsultasi='Rabu')
        booking_kamis = Booking.objects.filter(dokter=doctor, hari_konsultasi='Kamis')
        booking_jumat = Booking.objects.filter(dokter=doctor, hari_konsultasi='Jumat')
        response = {'booking_senin': booking_senin, 'booking_selasa': booking_selasa, 'booking_rabu': booking_rabu,
                    'booking_kamis': booking_kamis, 'booking_jumat': booking_jumat}
        return render(request, 'consultation_doctor.html', response)
    return redirect('/consultation')


@login_required(login_url='/accounts/login')
def consultation_form(request):
    form = ConsultationForm()
    if request.method == 'POST':
        form = ConsultationForm(request.POST)
        if form.is_valid():
            form.save()

        return redirect('/consultation/')
    return render(request, 'consultation_form.html', {'form': form})


def search_consultation(request):
    if request.method == 'POST':
        search_str = json.loads(request.body).get('searchText')
        if len(search_str) == 0:
            dokter = Profile.objects.all()
        else:
            try:
                dokter = Profile.objects.get(name__istartswith=search_str, role='doctor')
            except:
                dokter = None

        consultation = Consultation.objects.filter(dokter=dokter)
        data = consultation.values()
        print(type(dokter))
        print(type(consultation))
        print(type(data), data)
        return JsonResponse(list(data), safe=False)


def delete_booking(request, pk):
    booking = Booking.objects.filter(pk=pk)
    booking.delete()

    doctor = Profile.objects.filter(user=request.user)[0]
    booking = Booking.objects.filter(dokter=doctor)
    response = {'booking': booking}
    return render(request, 'consultation_doctor.html', response)


def flutter_get_profile(request):
    doctor_name = Profile.objects.all()
    data = serializers.serialize('json', doctor_name)
    return HttpResponse(data, content_type="application/json")

def flutter_cek_role(request):
    profile = Profile.objects.filter(user=request.user)
    # cek = False
    # if (profile.role != None and profile.role == 'Doctor'):
    #     cek = True
    data = serializers.serialize('json', profile)
    return HttpResponse(data, content_type="application/json")

def flutter_consultation_patient(request):
    consultation_data = Consultation.objects.all()
    data = serializers.serialize('json', consultation_data)
    print(data)
    return HttpResponse(data, content_type="application/json")


def flutter_consultation_doctor(request):
    doctor = Profile.objects.filter(user=request.user)[0]
    if doctor.role == 'doctor':
        consultation_data = Booking.objects.filter(dokter=doctor)
        data = serializers.serialize('json', consultation_data)
        print(data)
        return HttpResponse(data, content_type="application/json")

@csrf_exempt
def flutter_consultation_form(request):
    if request.method == 'POST':
        print(request.user)
        data = json.loads(request.body)
        
        

        return JsonResponse({"status": "success"}, status=200)
    else:
        return JsonResponse({"status": "error"}, status=401)

@csrf_exempt
def flutter_delete_booking(request):
    if request.method == 'POST':
        print(request.user)
        data = json.loads(request.body)

        return JsonResponse({"status": "success"}, status=200)
    else:
        return JsonResponse({"status": "error"}, status=401)

@csrf_exempt
def flutter_search_consultation(request):
    if request.method == 'POST':
        print(request.user)
        data = json.loads(request.body)

        return JsonResponse({"status": "success"}, status=200)
    else:
        return JsonResponse({"status": "error"}, status=401)