from django.shortcuts import render
from rest_framework import generics, filters
# from rest_framework import permissions
from forum import models as modelForum
from .serializers import ForumSerializer


class ListForum(generics.ListCreateAPIView):
    search_fields = ['judul', 'isi', 'namaPenulis']
    filter_backends = (filters.SearchFilter,)
    queryset = modelForum.Post.objects.order_by('-DateTime')
    serializer_class = ForumSerializer

class DetailForum(generics.RetrieveUpdateDestroyAPIView):
    queryset = modelForum.Post.objects.all()
    serializer_class = ForumSerializer 