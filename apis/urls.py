from django.urls import path

from .views import ListForum, DetailForum

urlpatterns = [
    path('forum/', ListForum.as_view()),
    path('forum/<int:pk>/', DetailForum.as_view()) 
]

