from rest_framework import serializers
from forum import models as modelForum


class ForumSerializer(serializers.ModelSerializer):
    class Meta:
        fields = (
            'id',
            'judul',
            'kategori',
            'isi',
            'DateTime',
            'warna',
            'namaPenulis',
            'penulis',
            'strings',
        )
        model = modelForum.Post

