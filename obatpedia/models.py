from django.db import models


# Create your models here.
class Medicine(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    composition = models.TextField()
    dosage_instructions = models.TextField()
    side_effects = models.TextField()
    image = models.ImageField(upload_to='medicine', default='medicine/jill.gif')

    def __str__(self):
        return self.name
