from django.shortcuts import render
from obatpedia.models import Medicine
from obatpedia.forms import MedicineForm
from django.http.response import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from authentication.models import Profile
import json
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import base64
from django.core.files.base import ContentFile

# Create your views here.
def guest_view(request):
    medicines = Medicine.objects.all()
    response = {"medicines": medicines,
                "qs_json": json.dumps(list(Medicine.objects.values())),
                }
    if request.user.is_authenticated:
        current_user = Profile.objects.get(user=request.user)
        user_role = current_user.role
        response['user_role'] = user_role
        print(user_role)
    return render(request, 'obatpedia/main.html', response)


@login_required(login_url='/accounts/login')
def add_medicine(request):
    current_user = Profile.objects.get(user=request.user)
    print(current_user.role)
    if current_user.role == "patient":
        return HttpResponseRedirect('/obatpedia/')
    form = MedicineForm(request.POST, request.FILES)
    response = {'form': form}
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/obatpedia/')
    # medicine = Medicine.objects.all()
    # response['medicine'] = medicine
    return render(request, 'obatpedia/add_medicine.html', response)


def search_results(request):
    if request.is_ajax():
        result = None
        medicine = request.POST.get('medicine')
        print(medicine)
        qs = Medicine.objects.filter(name__icontains=medicine)
        print(qs)
        if len(qs) > 0:
            data = []
            for index in qs:
                item = {
                    'pk': index.pk,
                    'name': index.name,
                    'image': str(index.image.url),
                    'description': index.description,
                    'composition': index.composition,
                    'dosage_instructions': index.dosage_instructions,
                    'side_effects': index.side_effects,
                }
                data.append(item)
            result = data
        else:
            result = "No medicine  found."

        return JsonResponse({'data': result})
    return JsonResponse({})

def http_response(request):
    medicines = Medicine.objects.all()
    medicine_response = serializers.serialize('json', medicines)
    return HttpResponse(medicine_response, content_type="application/json")

@csrf_exempt
def add_medicine_flutter(request):
    if request.method == 'POST':
        data = json.loads(request.body)

        name_flutter = data["name"]
        description_flutter = data["description"]
        composition_flutter = data["composition"]
        dosage_instructions_flutter = data["dosage_instructions"]
        side_effects_flutter = data["side_effects"]

        base64flutter = data["base64image"]
        print("The base64 thingy: ")
        print(base64flutter)
        if (base64flutter == "no image"):
            medicine_form = Medicine(name=name_flutter, description=description_flutter, composition=composition_flutter,
                                     dosage_instructions=dosage_instructions_flutter, side_effects=side_effects_flutter)
        else:
            image_flutter = ContentFile(base64.b64decode(base64flutter), name=f"{name_flutter}flutter")
            medicine_form = Medicine(name=name_flutter, description=description_flutter,
                                     composition=composition_flutter, image=image_flutter,
                                     dosage_instructions=dosage_instructions_flutter, side_effects=side_effects_flutter)
        medicine_form.save()

        return JsonResponse({"status": "success"}, status=200)
    else:
        return JsonResponse({"status": "error"}, status=401)



