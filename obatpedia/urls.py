from django.urls import path
from obatpedia.views import guest_view, add_medicine, search_results, http_response, add_medicine_flutter

app_name = 'obatpedia'

urlpatterns = [
    path('', guest_view, name='guest view'),
    path('add', add_medicine, name='add medicine'),
    path('search/', search_results, name='search results'),
    path('http_response', http_response, name='http medicine response'),
    path('addMedicineFlutter', add_medicine_flutter, name="post medicine flutter")
]
