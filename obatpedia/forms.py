from django import forms
from obatpedia.models import Medicine


class MedicineForm(forms.ModelForm):
    class Meta:
        model = Medicine
        fields = ['name', 'image', 'description', 'composition',
                  'dosage_instructions', 'side_effects']
