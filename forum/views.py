from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from rest_framework.serializers import Serializer
from django.shortcuts import get_object_or_404, redirect, render
from .forms import PostForm, CommentForm, ReplyCommentForm
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from .models import Post, Comment, ReplyComment, Color
from authentication.models import Profile
from django.contrib.auth.models import User
from django.core import serializers
import json 

def forum(request, category=None):
    num_posts = Post.objects.all().count()
    num_users = User.objects.all().count()
    colorProfile = Color.objects.all()
    
    if request.user.is_authenticated and category == 'user' :
        diskusi = Post.objects.filter(penulis = request.user).order_by('-DateTime')
    elif category != 'all' and category:
        diskusi = Post.objects.filter(kategori = category).order_by('-DateTime')
    else:
        diskusi = Post.objects.all().order_by('-DateTime')
        
    if category==None:
        category = 'all'
        
    return render(request, 'forum/mainForum.html',{
        "num_posts":num_posts,
        'category' : category,
        "num_users":num_users,
        'diskusi' : diskusi, 
        'colorProfile' : colorProfile, 
    })


def profil(request, username):
    num_posts = Post.objects.all().count()
    num_users = User.objects.all().count()
    #check if user exist
    filterUser = User.objects.filter(username = username)
    
    if filterUser:
        user = User.objects.get(username = username)

        #check if user already have color profile
        filter = Color.objects.filter(user = user)
        
        diskusi = Post.objects.filter(penulis = user).order_by('-DateTime')
        colorProfile = Color.objects.all()
        
        if filter:
            warna = Color.objects.get(user = user)
        
        if request.user == user:
            if not filter:
                warna = Color(user = user)
                
            if request.POST and form.is_valid():
                print(request.POST)
                
                posts = Post.objects.all()
                for post in posts:
                    tempWarna = Color.objects.get(user = post.penulis)
                    post.warna = tempWarna.colorHex
                    post.save()
                return HttpResponseRedirect(request.path_info)
            
        if not filter:
            warna = None
        
        return render(request, 'forum/profil.html', {
            "num_posts":num_posts,
            "num_users":num_users,
            'userInProfil' : user, 
            'warna' : warna, 
            'diskusi' : diskusi, 
            'colorProfile' : colorProfile
        })


@login_required(login_url='/accounts/login')
def editPost(request, pk = None):
    'pk is the attribute that contains the value of the primary key for the model.'
    if pk:
        post = get_object_or_404(Post, pk=pk)
    else:
        post = Post(penulis = request.user)

    # Create a form instance with POST data.
    form = PostForm(request.POST or None, instance = post)
    if request.POST and form.is_valid():
        print(request.POST)
        # Save the new instance.
        form.save()
        current_user = Profile.objects.get(user=request.user)
        warna = Color.objects.filter(user = request.user)
        if not warna:
            warna = Color(
                colorHex = current_user.profil_color,
                user = request.user
            )
            warna.save()
        
        temp = Post.objects.last()
        tempWarna = Color.objects.get(user = temp.penulis)
        temp.warna = tempWarna.colorHex
        print(temp.warna)
        lststr = [temp.judul,temp.penulis.username,temp.isi]
        temp.strings = ' '.join(lststr) 
        temp.namaPenulis = temp.penulis.username
        temp.save()

        return redirect('forum:forum1')

    context = {'form': form}
    return render(request, 'forum/editPost.html',context)
   

def readForum(request, pk = None):
    diskusi = get_object_or_404(Post, pk = pk)
    komen = Comment.objects.filter(post = diskusi).order_by('DateTime')
    reply = ReplyComment.objects.all().order_by('DateTime')

    if request.user.is_authenticated:
        komenForForm = Comment(penulis = request.user, post = diskusi)
        form = CommentForm(request.POST or None, instance = komenForForm)
        if request.POST and form.is_valid():
            print(request.POST)
            form.save()
            
            kom = Comment.objects.last()
            kom.namaPenulis = kom.penulis.username
            kom.warna = Color.objects.get(user = kom.penulis).colorHex
            kom.jumlahReply = 0
            kom.save()
            
            return HttpResponseRedirect(request.path_info)
    else:
        form = None
        
    colorProfile = Color.objects.all()
    return render(request, 'forum/readForum.html',{
        'diskusi' : diskusi, 
        'komen' : komen, 
        'reply' : reply, 
        'form' : form, 
        'colorProfile' : colorProfile
    })




@login_required(login_url='/accounts/login')
def editComment(request, pk = None):
    if request.user.is_authenticated:
    
        komen = get_object_or_404(Comment, pk=pk)
                
        form = CommentForm(request.POST or None, instance = komen)
        if request.POST and form.is_valid():
            print(request.POST)
            form.save()
            return redirect('forum:readForum', komen.post.pk)
        
        colorProfile = Color.objects.all()
        return render(request, 'forum/editComment.html',{
            'form': form,
            'diskusi' : komen.post,
            'komen' : komen,
            'type' : 'edit',
            'colorProfile' : colorProfile
        })


@login_required(login_url='/accounts/login')
def editReply(request, pkC = None, pkR = None):
    target = None
    
    if not pkC and pkR:
        target = get_object_or_404(ReplyComment, pk=pkR)
        komen = target.komen
    else:
        komen = get_object_or_404(Comment, pk=pkC)

    if pkC and pkR:
        reply = get_object_or_404(ReplyComment, pk=pkR)
        target = reply
        type = 'edit'
    else:
        reply = ReplyComment(penulis = request.user, komen = komen)
        type = 'add'

    form = ReplyCommentForm(request.POST or None, instance = reply)
    if request.POST and form.is_valid():
        print(request.POST)
        form.save()
        if type == 'add':
            rep = ReplyComment.objects.last()
            rep.namaPenulis = rep.penulis.username
            rep.warna = Color.objects.get(user = rep.penulis).colorHex
            rep.save()
            kom = rep.komen
            kom.jumlahReply = int(kom.jumlahReply) + 1
            kom.save()
        return redirect('forum:readForum', komen.post.pk)
    
    # target
    if target != None:
        replyTo = [target]
        while target.targetBalasan != None:
            replyTo.append(target.targetBalasan)
        replyTo.reverse()
    else:
        replyTo = None
    
    colorProfile = Color.objects.all()
    
    context = {
        'form': form,
        'komen' : komen,
        'replyTo' : replyTo,
        'type' : type,
        'colorProfile' : colorProfile
    }
    return render(request, 'forum/editComment.html', context)
    
    
def confirmDeletePost(request, pk = None):
    if request.user.is_authenticated:
        post = get_object_or_404(Post, pk=pk)
        colorProfile = Color.objects.all()
        return render(request, 'forum/confirmDelete.html', {
            'diskusi':post, 
            'colorProfile' : colorProfile
        })
    

def deletePost(request, pk = None):
    if request.user.is_authenticated:
        post = Post.objects.filter(pk=pk)
        post.delete()
        return redirect('forum:forum1')
    
    
def confirmDeleteComment(request, pk = None):
    if request.user.is_authenticated:
        comment = get_object_or_404(Comment, pk=pk)
        colorProfile = Color.objects.all()
        
        return render(request, 'forum/confirmDelete.html', {
            'komen':comment, 
            'colorProfile' : colorProfile
        })


def deleteComment(request, pk = None):
    if request.user.is_authenticated:
        komen = Comment.objects.filter(pk=pk)
        post = komen[:1].get().post
        komen.delete()
        return redirect('forum:readForum', post.pk)
      

def confirmDeleteReply(request, pk = None):
    if request.user.is_authenticated:
        colorProfile = Color.objects.all()
        target = get_object_or_404(ReplyComment, pk=pk)
        reply = target
        komen = reply.komen
        
        #target
        if target != None:
            replyTo = [target]
            while target.targetBalasan != None:
                target = target.targetBalasan
                replyTo.append(target) 
        else:
            replyTo = None

        return render(request, 'forum/confirmDelete.html', {
            'reply':reply, 
            'komen':komen, 
            'replyTo':replyTo, 
            'colorProfile' : colorProfile
        })
    


def deleteReply(request, pk = None):
    if request.user.is_authenticated:
        reply = ReplyComment.objects.filter(pk=pk)
        post = reply[:1].get().komen.post
            
        kom = reply[:1].get().komen
        print(kom)
        reply.delete()
        
        count = 0
        allReply = ReplyComment.objects.all()
        for reply in allReply:
            if reply.komen == kom:
                count = count + 1
        kom.jumlahReply = count
        kom.save()
        
        return redirect('forum:readForum', post.pk)
    

def search(request, category = None): 
    if category == 'user':
        diskusi = Post.objects.filter(penulis = request.user).order_by('-DateTime')
    elif category != 'all' and category:
        diskusi = Post.objects.filter(kategori = category).order_by('-DateTime')
    else:
        diskusi = Post.objects.all().order_by('-DateTime')
    
    input = request.GET['c']
    selected = request.GET['q']
    temp = 0
    lstObject = []
    index = []
    if selected == 'all':
        for disk in diskusi:
            temp = searchWord(disk.strings.lower(), input.lower())
            if temp != -1:
                lstObject.append(disk)
                index.append(temp)
    elif selected == 'isi':
        for disk in diskusi:
            temp = searchWord(disk.isi.lower(), input.lower())
            if temp != -1:
                lstObject.append(disk)
                index.append(temp)
    elif selected == 'penulis':
        for disk in diskusi:
            temp = searchWord(disk.penulis.username.lower(), input.lower())
            if temp != -1:
                lstObject.append(disk)
                index.append(temp)
    elif selected == 'judul':
        for disk in diskusi:
            temp = searchWord(disk.judul.lower(), input.lower())
            if temp != -1:
                lstObject.append(disk)
                index.append(temp)

    result1 = sorted(list(zip(index, lstObject)), key=lambda obj : obj[0], reverse=True)
    
    result = []
    for res in result1:
        result.append(res[1])
    '''
    When use_natural_foreign_keys=True is specified, 
    Django will use the natural_key() method to serialize any foreign key 
    reference to objects of the type that defines the method.

    When use_natural_primary_keys=True is specified, 
    Django will not provide the primary key in the serialized 
    data of this object since it can be calculated during deserialization
    https://docs.djangoproject.com/en/3.2/topics/serialization/
    '''
    data = serializers.serialize('json', 
            result, 
            use_natural_foreign_keys=True, 
            use_natural_primary_keys=True)
    return HttpResponse(data, content_type='application/json')
     

def searchWord(text, word):
    index = 0
    counter = -1
    while index < len(text):
        index = text.find(word, index)
        if index == -1:
            break
        index = index + len(word)
        counter = 1 + counter
    return counter

def getCommentForum(request, pk = None):
    diskusi = get_object_or_404(Post, pk = pk)
    komen = Comment.objects.filter(post = diskusi).order_by('DateTime').reverse()
    data = serializers.serialize('json', komen)
    return HttpResponse(data, content_type="application/json")  

def getReplyForum(request, pk = None):
    comment = get_object_or_404(Comment, pk = pk)
    reply = ReplyComment.objects.filter(komen = comment).order_by('DateTime').reverse()
    data = serializers.serialize('json', reply)
    return HttpResponse(data, content_type="application/json")  

def getCategoryForum(request, category=None):
    if category == 'user' :
        diskusi = Post.objects.filter(penulis = request.user).order_by('-DateTime')
    elif category != 'all' and category:
        diskusi = Post.objects.filter(kategori = category).order_by('-DateTime')
    else:
        diskusi = Post.objects.all().order_by('-DateTime')
    
    data = serializers.serialize('json', diskusi)
    return HttpResponse(data, content_type="application/json") 

def getNum(request,category=None):
    if category == 'user' :
        num_posts = Post.objects.filter(penulis = request.user).count()
    elif category != 'all' and category:
        num_posts = Post.objects.filter(kategori = category).count()
    else:
        num_posts = Post.objects.all().count()
    my_json = {}
    my_json['num_posts'] = num_posts
    my_json['num_users'] = num_users
    data = json.dumps(my_json)
    return HttpResponse(data, content_type="application/json")

@csrf_exempt
def postNewForum(request):
    if request.method == 'POST':
        print(request.user)
        data = json.loads(request.body)

        judul_flutter = data["judul"]
        kategori_flutter = data["kategori"]
        isi_flutter = data["isi"]
        penulis_flutter = request.user

        Post_form = Post(judul=judul_flutter, kategori=kategori_flutter, 
                            isi=isi_flutter, penulis=penulis_flutter)
        Post_form.save()

        current_user = Profile.objects.get(user=request.user)
        warna = Color.objects.filter(user = request.user)
        if not warna:
            warna = Color(
                colorHex = current_user.profil_color,
                user = request.user
            )
            warna.save()
        
        temp = Post.objects.last()
        tempWarna = Color.objects.get(user = temp.penulis)
        temp.warna = tempWarna.colorHex
        lststr = [temp.judul,temp.penulis.username,temp.isi]
        temp.strings = ' '.join(lststr) 
        temp.namaPenulis = temp.penulis.username
        temp.save()

        return JsonResponse({"status": "success"}, status=200)

    else:
        return JsonResponse({"status": "error"}, status=401)


@csrf_exempt
def commentNewForum(request, pk = None):
    if request.method == 'POST':
        print(request.user)
        data = json.loads(request.body)

        isi_flutter = data["isi"]
        post_flutter = get_object_or_404(Post, pk = pk)
        penulis_flutter = request.user
        Post_form = Comment(isi=isi_flutter, penulis=penulis_flutter, post = post_flutter)
        Post_form.save()

        current_user = Profile.objects.get(user=request.user)
        warna = Color.objects.filter(user = request.user)
        if not warna:
            warna = Color(
                colorHex = current_user.profil_color,
                user = request.user
            )
            warna.save()
        
        temp = Comment.objects.last()
        temp.namaPenulis = request.user.username
        temp.jumlahReply = 0
        tempWarna = Color.objects.get(user = temp.penulis)
        temp.warna = tempWarna.colorHex
        temp.save()

        return JsonResponse({"status": "success"}, status=200)

    else:
        return JsonResponse({"status": "error"}, status=401)


@csrf_exempt
def replyCommentNewForum(request, pk = None):
    if request.method == 'POST':
        print(request.user)
        data = json.loads(request.body)

        isi_flutter = data["isi"]
        penulis_flutter = request.user
        comment_flutter = get_object_or_404(Comment, pk = pk)
        comment_flutter.jumlahReply = int(comment_flutter.jumlahReply) + 1
        comment_flutter.save()
        Post_form = ReplyComment(isi=isi_flutter, penulis=penulis_flutter, komen = comment_flutter)
        Post_form.save()

        current_user = Profile.objects.get(user=request.user)
        warna = Color.objects.filter(user = request.user)
        if not warna:
            warna = Color(
                colorHex = current_user.profil_color,
                user = request.user
            )
            warna.save()
        
        temp = ReplyComment.objects.last()
        temp.namaPenulis = request.user.username
        tempWarna = Color.objects.get(user = temp.penulis)
        temp.warna = tempWarna.colorHex
        temp.save()

        return JsonResponse({"status": "success"}, status=200)

    else:
        return JsonResponse({"status": "error"}, status=401)
 