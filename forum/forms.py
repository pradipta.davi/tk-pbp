from django import forms
from .models import Post, Color, Comment, ReplyComment
from forum.models import KATEGORI

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ["judul", "kategori", "isi"]
    
    input_judul = {
        'type' : 'text',
        'placeholder' : 'Title Discussion',
        'class' : 'form-control',
        'autocomplete' : 'off'
    }
    
    input_kategori = {
        'type' : 'text',
        'placeholder' : 'Category Discussion',
        'class' : 'form-control arrow'
    }
    
    input_isi = {
        'type' : 'text',
        'placeholder' : 'Write Here',
        'class' : 'form-control'
    }
    
    judul = forms.CharField(label='', required=True, max_length=150, widget=forms.TextInput(attrs=input_judul))
    kategori = forms.ChoiceField(label='', choices=KATEGORI, widget=forms.Select(attrs=input_kategori))
    isi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_isi))

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ["isi"]
 
    input_isi = {
        'type' : 'text',
        'placeholder' : 'Write Here',
        'class' : 'form-control',
        'autocomplete' : 'off',
        'rows' : 3,
    }
    
    isi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_isi))

    
class ReplyCommentForm(forms.ModelForm):
    class Meta:
        model = ReplyComment
        fields = ["isi"]
    
    input_isi = {
        'type' : 'text',
        'placeholder' : 'Reply Here',
        'class' : 'form-control',
        'autocomplete' : 'off',
        'rows' : 3,
    }
    
    isi = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=input_isi))
    
    