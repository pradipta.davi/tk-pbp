from django.urls import path

from . import views

app_name = 'forum'

urlpatterns = [
    path('', views.forum, name='forum1'),
    path('category/<str:category>/', views.forum, name='forum2'),
    path('category/<str:category>/search/', views.search),
    path('posts/<int:pk>/', views.readForum, name='readForum'),
    
    path('addPost/', views.editPost, name='addPost'),
    path('addReply/<int:pkC>/', views.editReply, name='addReply'),

    path('editPost/<int:pk>/', views.editPost, name='editPost'),
    path('editComment/<int:pk>/', views.editComment, name='editComment'),
    path('editReply/<int:pkC>/<int:pkR>/', views.editReply, name='editReply'),
    
    path('deletePost/<int:pk>/', views.deletePost, name='deletePost'),
    path('deleteComment/<int:pk>/', views.deleteComment, name='deleteComment'),
    path('deleteReply/<int:pk>/', views.deleteReply, name='deleteReply'),
    
    path('profil/<str:username>/', views.profil, name='profil'),
    
    path('confirmDeletePost/<int:pk>/', views.confirmDeletePost, name='confirmDeletePost'),
    path('confirmDeleteComment/<int:pk>/', views.confirmDeleteComment, name='confirmDeleteComment'),
    path('confirmDeleteReply/<int:pk>/', views.confirmDeleteReply, name='confirmDeleteReply'),
    
    path('search/', views.search),

    path('commentForum/<int:pk>/', views.getCommentForum, name='commentForum'),
    path('replyCommentForum/<int:pk>/', views.getReplyForum, name='replyCommentForum'),
    path('api/<str:category>/', views.getCategoryForum, name='getCategoryForum'),
    path('getNum/', views.getNum, name='getNum'),
    
    path('postNewForum/', views.postNewForum, name='postNewForum'),
    path('commentNewForum/<int:pk>/', views.commentNewForum, name='commentNewForum'),
    path('replyCommentNewForum/<int:pk>/', views.replyCommentNewForum, name='replyCommentNewForum'), 
]