from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now
from datetime import datetime, date

KATEGORI = (
    ('', '*Choose Category Discussion*'),
    ('general', 'General Discussion'),
    ('covid', 'Covid Info'),
    ('drug', 'Drug Info'),
)

class Post(models.Model):
    judul = models.CharField(max_length = 150)
    kategori = models.CharField(max_length = 25, choices = KATEGORI, default='')
    isi = models.TextField()
    """
    When the referenced object is deleted, 
    also delete the objects that have references
    to it (when you remove a blog post for instance, 
    you might want to delete comments as well)
    """
    penulis = models.ForeignKey(User, on_delete = models.CASCADE,blank=True, null=True)
    DateTime = models.DateTimeField(default=now)
    warna = models.CharField(max_length = 10,default='#bcbcbc')
    strings = models.TextField()
    namaPenulis = models.CharField(max_length = 50, default='')

    @property
    def num_posts(self):
        return Post.objects.filter(kategori=self).count()
    
class Color(models.Model):
    # Many-to-one relationships
    user = models.ForeignKey(User, on_delete = models.CASCADE)
    colorHex = models.CharField(max_length = 10,blank=True,default='#bcbcbc')
    
class Comment(models.Model):
    isi = models.TextField()
    post = models.ForeignKey(Post, on_delete = models.CASCADE)
    penulis = models.ForeignKey(User, on_delete = models.CASCADE)
    namaPenulis = models.CharField(max_length = 50, default='')
    DateTime = models.DateTimeField(default=now)
    jumlahReply = models.CharField(max_length = 100)
    warna = models.CharField(max_length = 10,default='#bcbcbc')
    
class ReplyComment(models.Model):
    isi = models.TextField()
    penulis = models.ForeignKey(User, on_delete = models.CASCADE)
    namaPenulis = models.CharField(max_length = 50, default='')
    komen = models.ForeignKey(Comment, on_delete = models.CASCADE)
    targetBalasan = models.ForeignKey('self', on_delete = models.CASCADE, null=True)
    DateTime = models.DateTimeField(default=now)
    warna = models.CharField(max_length = 10,default='#bcbcbc')
 