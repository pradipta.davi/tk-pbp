$(document).ready(function(){
    $(".reply-container").hide();
    $(".showRepliesChild2").hide();
    $(".showRepliesChild1").on('click', function (d){
        $(this).hide();
        $(this).next().show();
        $(this).parent().next().show();
    });
    
    $(".showRepliesChild2").click(function(){
        $(this).hide();
        $(this).prev().show();
        $(this).parent().next().hide();
    });
    
});