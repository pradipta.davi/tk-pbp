
$(document).ready(function() {
    
    var pathname = window.location.pathname;
    var param = pathname.slice(-7, -1);
    // console.log(param);
    var category;
    var categorySearch;
            
    if (param == 'ry/all' || param == '/forum') {category = 'all'; categorySearch = 'All Category';} 
    else if (param == 'eneral') {category = 'general'; categorySearch = 'General Discussion';} 
    else if (param == '/covid') {category = 'covid'; categorySearch = 'Covid Info';} 
    else if (param == 'y/drug') {category = 'drug'; categorySearch = 'Drug Info';} 
    else if (param == 'y/user') {category = 'user'; categorySearch = 'My Discussion';}
    
    $(".drpdwnSearch-content").append(
        "<div id='searchStart' class='text-center'>" +
            "What do you want to find?" +
            "<br> You are looking for section <strong>'" + categorySearch + "'</strong>." +
        "</div>"
    );
    
    $('#load').hide();
    $('#load2').hide();
    $(".drpdwnSearch-content").hide();
    
    $("#inputSearch").focusin(function(){
        $(".drpdwnSearch-content").fadeIn(250);
    });
    
    $("#inputSearch").focusout(function(){
        $(".drpdwnSearch-content").fadeOut(250);
    });
    
    var temp1;
    var fired = false;

    $('#inputSearch').onkeydown = function() {
        if(!fired) {
            fired = true;
            temp1 = $('#inputSearch').val();
        }
    };
    $('#inputSearch').onkeyup = function() {
        fired = false;
    };
    
    var globalTimeout = null;  
    $('#inputSearch').keyup(function(d){
        var temp2 = $('#inputSearch').val();
        if (temp1 == temp2) {
             return;
        }
        
        if((d.keyCode == 8 || d.keyCode == 46) && !($("#inputSearch").val())) {
            $(".drpdwnSearch-content").empty();
            $('#load2').hide();
            $(".drpdwnSearch-content").append(
                "<div id='searchStart'>" +
                    "What do you want to find?" +
                    "<br> You are looking for section <strong>'" + categorySearch + "'</strong>." +
                "</div>"
            );
        } else {
            $(".drpdwnSearch-content").empty();
            $('#load2').show();
        }
        
        if (globalTimeout != null) {
            clearTimeout(globalTimeout);  
        }
        globalTimeout = setTimeout(searchFunct, 500);  
    });
    
    $('#select').change(function() {
        if ($("#inputSearch").val()) {
            $(".drpdwnSearch-content").empty();        
            $('#load2').show();
            searchFunct();    
        }
    });
    
    function searchFunct() {
        globalTimeout = null;
        
        var input = $("#inputSearch").val();
        var selected = $('#select').val();
        console.log(input);
        
        $.ajax({
            url : 'search/?c=' + input + '&q=' + selected,
            success : function(data) {
                $(".drpdwnSearch-content").empty();
                console.log(data);
                
                $(".drpdwnSearch-content").append("<br>You are looking for section <strong>" + categorySearch + "</strong><br>");
                
                for (i = 0; i < data.length; i++) {
                    var classKategori;
                    var byCategory;
                    var day;
                    var month;
                    var year;
                    var time;
                    day = data[i].fields.DateTime.slice(8,10);
                    if      (data[i].fields.DateTime.slice(5,7)==01) {month = 'Jan';}
                    else if (data[i].fields.DateTime.slice(5,7)==02) {month = 'Feb';}
                    else if (data[i].fields.DateTime.slice(5,7)==03) {month = 'Mar';}
                    else if (data[i].fields.DateTime.slice(5,7)==04) {month = 'Apr';}
                    else if (data[i].fields.DateTime.slice(5,7)==05) {month = 'May';}
                    else if (data[i].fields.DateTime.slice(5,7)==06) {month = 'Jun';}
                    else if (data[i].fields.DateTime.slice(5,7)==07) {month = 'Jul';}
                    else if (data[i].fields.DateTime.slice(5,7)==08) {month = 'Aug';}
                    else if (data[i].fields.DateTime.slice(5,7)==09) {month = 'Sep';}
                    else if (data[i].fields.DateTime.slice(5,7)==10) {month = 'Oct';}
                    else if (data[i].fields.DateTime.slice(5,7)==11) {month = 'Nov';}
                    else if (data[i].fields.DateTime.slice(5,7)==12) {month = 'Dec';}
                    year = data[i].fields.DateTime.slice(0,4);
                    time = data[i].fields.DateTime.slice(11,16);

                    if (data[i].fields.kategori == 'general')  {classKategori = 'tipe1'; byCategory = '[Gneral Discussion]';}
                    else if (data[i].fields.kategori == 'covid') {classKategori = 'tipe2';byCategory = '[Covid Info]';}
                    else if (data[i].fields.kategori == 'drug') {classKategori = 'tipe3';byCategory = '[Drug Info]';}
                    
                    var warnas = data[i].fields.warna;
                    console.log(warnas)
                    var loca = "location.href='/forum/posts/" + data[i].pk + "/';"
                    console.log(data[i].fields.kategori)
                    var res = "<div class='post' onclick=" + loca  + "id='searchDivs'>" +
                            "<div class='topdown inside-post center b'>" +
                                    "<div class='profile' style='background-color: " + warnas + ";'>" + 
                                        "<div class='topdown center inisial'>" +
                                            data[i].fields.penulis[0].charAt(0).toUpperCase() +
                                        "</div>" +
                                    "</div>" +
                                "<div class='topdown snippet fontall'>" +
                                    "<div class='judulPost b'>" +
                                        data[i].fields.judul.slice(0,110) + "<br>" +
                                    "</div>" +
                                    "<div class='subJudulPost text-center'>" +
                                        "<a href='/forum/category/" + data[i].fields.kategori + "/' class='" + classKategori + "'>" +
                                            byCategory +
                                        "</a> by " + 
                                        "<a href='/forum/profil/" + data[i].fields.penulis[0] + "' class='edit'>" + data[i].fields.penulis + "</a>" +
                                         " on " + day +"-" + month + "-" + year +" "+ time +" WIB";
                    
                    
                    if (data[i].fields.penulis[0] == myForum) {
                        res = res + " | " + "<a href='/forum/editPost/" + data[i].pk + "' class='edit'> edit </a> | " +
                            "<a href='/forum/confirmDeletePost/" + data[i].pk + "' class='edit'> delete </a>";
                    }
                        
                                                           
                        res = res + "</div>" +
                                    "<div class='text-center b'>" +
                                        data[i].fields.isi.slice(0,110) + "..." +
                                    "</div>" +
                                "</div>" +
                            "</div>" +
                        "</div>";
                     
                    $(".drpdwnSearch-content").append(res);
                }
                
                $(".drpdwnSearch-content").append("<br>");
                $('#load2').hide();
                
                if (data.length == 0 ) {
                    $(".drpdwnSearch-content").empty();
                    $(".drpdwnSearch-content").append(
                        "<div id='searchStart'>" +
                            "Sorry, but <strong>'" + input + "'</strong> not Found :(" +
                        "</div>"
                    );
                }
            }
        });
    }
    
});