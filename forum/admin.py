from django.contrib import admin
from .models import Post, Color, Comment, ReplyComment
# Register your models here.
admin.site.register(Post)
admin.site.register(Color)
admin.site.register(Comment)
admin.site.register(ReplyComment)