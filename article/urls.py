from django.urls import path
from .views import *
app_name = 'article'

app_name = 'article'

urlpatterns = [
    path('', index, name='article'),
    # TODO Add friends path using friend_list Views
    path('add',add_artikel),
    path('<int:id>',artikel_detail,name='artikel_detail'),
    path('rate',rate,name="rate-view"),
    path('search', search_results, name='search results'),
    path('getArtikelFlutter',getArtikelFlutter,name='getArtikelFlutter'),
    path('addArtikelFlutter',addArtikelFlutter,name='addArtikelFlutter'),
]
