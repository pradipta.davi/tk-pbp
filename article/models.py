from django.db import models
from django.core.validators import MaxValueValidator,MinValueValidator
from django.utils.timezone import now


# Create your models here.
class Artikel(models.Model):
    penulis=models.CharField(max_length=30,default='anonymous')
    judul = models.CharField(max_length=200)
    isi = models.TextField(blank = True)
    datetime = models.DateTimeField(default=now)
    
    rating = models.IntegerField(default = 0,
        validators =[MaxValueValidator(5),MinValueValidator(0),]
        )

    def __str__(self):
        return self.judul