from django.shortcuts import render
from rest_framework.serializers import Serializer
from .models import Artikel
from .forms import ArtikelForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.core import serializers
import json
from django.http.response import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def index(request):
    artikel = Artikel.objects.all()  # TODO Implement this
    response = {'artikel': artikel}
    return render(request, 'homeArtikel.html', response)

@login_required(login_url='/accounts/login')
def add_artikel(request):
    context = {}
    form = ArtikelForm(request.POST or None)
    if(request.method == 'POST') and (form.is_valid()):    
        form.save()
        return HttpResponseRedirect('/article')
        
    context['form'] = form
    return render(request, 'formArtikel.html', context)

@login_required(login_url='/accounts/login')
def artikel_detail(request, id):  # Untuk readmore
    artikel = get_object_or_404(Artikel,pk=id)
    response = {'artikel': artikel}
    return render(request, 'Artikel_details.html', response)

@login_required(login_url='/accounts/login')
def rate(request):
    if request.method == 'POST':
        el_id = request.POST.get('el_id')
        val = request.POST.get('val')
        obj = Artikel.objects.get(id=el_id)
        obj.rating = val
        obj.save()
        return JsonResponse({'success': 'true', 'rating': val}, safe=False)
    return JsonResponse({'success': 'false'})


def search_results(request):
    if request.is_ajax():
        result = None
        artikel = request.POST.get('artikel')
        qs = Artikel.objects.filter(judul__icontains=artikel)
        
        if len(qs) > 0:
            data = []
            for index in qs:
                item = {
                    'judul': index.judul,
                    'isi': index.isi,
                    'rating': index.rating,
                    'id':index.id,
                    'penulis':index.penulis,
                    'datetime':index.datetime
                    
                }
                data.append(item)
            result = data
        else:
            result = "No article  found."

        return JsonResponse({'data': result})
    return JsonResponse({})

def getArtikelFlutter(request):
    query = serializers.serialize('json',Artikel.objects.all())
    return HttpResponse(query,content_type="application/json")

@csrf_exempt
def addArtikelFlutter(request):
    if request.method == 'POST':
    
        data = json.loads(request.body)

        penulis_flutter = data["penulis"]
        judul_flutter = data["judul"]
        isi_flutter = data["isi"]

        Artikel_form = Artikel(penulis = penulis_flutter,judul = judul_flutter ,isi = isi_flutter)
        Artikel_form.save()

        return JsonResponse({"status": "success"}, status=200)
       
    else:
        return JsonResponse({"status": "error"}, status=401)