"""CovidConsult URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
import homepage.urls as homepage
import forum.urls as forum
import authentication.urls as authentication
import consultation.urls as consultation
import article.urls as article
import obatpedia.urls as obatpedia
import profilepage.urls as profilepage

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(homepage)),
    path('forum/', include(forum)),
    path('accounts/', include(authentication)),
    path('article/',include(article)),
    path('consultation/', include(consultation)),
    path('obatpedia/', include(obatpedia)),
    path('apis/', include('apis.urls')), 
    path('profile/', include(profilepage))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)