

window.onload=function(){
    const formData = document.getElementById('user-update')
    const name = document.getElementById('name')
    const dob = document.getElementById('dob')
    const email = document.getElementById('email')
    const number = document.getElementById('number')
    const saveButton = document.getElementById('save-button')

    const csrf = document.getElementsByName('csrfmiddlewaretoken')[0].value
    
    const updateData = (user) => {
        console.log('ini user' + user.dob)
        $.ajax({
            type : 'POST',
            url : 'ajax/update/',
            data : {
                'csrfmiddlewaretoken' : csrf,
                'username' : user.name,
                'dob' : user.dob,
                'email' : user.email,
                'number' : user.number,
            },
            success: (result) => {
                console.log(result)
                console.log(result.dob)
                var day = result.dob.slice(8,10);
                var month;
                if      (result.dob.slice(5,7)==01) {month = 'Jan';}
                else if (result.dob.slice(5,7)==02) {month = 'Feb';}
                else if (result.dob.slice(5,7)==03) {month = 'Mar';}
                else if (result.dob.slice(5,7)==04) {month = 'Apr';}
                else if (result.dob.slice(5,7)==05) {month = 'May';}
                else if (result.dob.slice(5,7)==06) {month = 'Jun';}
                else if (result.dob.slice(5,7)==07) {month = 'Jul';}
                else if (result.dob.slice(5,7)==08) {month = 'Aug';}
                else if (result.dob.slice(5,7)==09) {month = 'Sep';}
                else if (result.dob.slice(5,7)==10) {month = 'Oct';}
                else if (result.dob.slice(5,7)==11) {month = 'Nov';}
                else if (result.dob.slice(5,7)==12) {month = 'Dec';}
                var year = result.dob.slice(0,4);

                document.getElementById('name-data').innerHTML = `
                <div class="col-4"> Name </div>
                <div class="col-8"> ${result.name} </div>
                `
                document.getElementById('dob-data').innerHTML = `
                <div class="col-4"> Birth Date </div>
                <div class="col-8"> ${month}. ${day}, ${year} </div>
                `
                document.getElementById('email-data').innerHTML = `
                <div class="col-4"> E-Mail </div>
                <div class="col-8"> ${result.email} </div>
                `
                document.getElementById('number-data').innerHTML = `
                <div class="col-4"> Phone Number </div>
                <div class="col-8"> ${result.number} </div>
                `
            },
            error: (error) => {
                console.log('error')
            }
        })
    }
    
    saveButton.addEventListener('click', function() {
    console.log(name.value)
    console.log(dob.value)
    console.log(email.value)
    console.log(number.value)

    var data = {'name' : name.value,
                'dob' : dob.value,
                'email' : email.value,
                'number' : number.value}

    console.log(data)
    updateData({'name' : name.value,
        'dob' : dob.value,
        'email' : email.value,
        'number' : number.value})
    })

}
