from django.urls import path
from .views import profile, change_password, update_data, getProfilelFlutter, editProfileFlutter

app_name = 'profilepage'

urlpatterns = [
    path('', profile, name="profile-page"),
    path('change-password/', change_password, name="password-change"),
    path('ajax/update/', update_data, name="ajax"),
    path('getflutter/', getProfilelFlutter, name="get_flutter"),
    path('editflutter/', editProfileFlutter, name="edit_flutter"),
]
