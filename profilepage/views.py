from django.shortcuts import render, redirect
from django.views import generic
from authentication.models import Profile
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.http.response import JsonResponse
from django.core import serializers
import json
from django.http.response import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
# Create your views here.


@login_required(login_url='/accounts/login')
def profile(request):
    current_user = Profile.objects.get(user=request.user)

    response = {'user' : current_user}
    return render(request, 'profilepage/profilepage.html', response)


def update_data(request):
    current_user = request.user
    
    if request.is_ajax():

        name = current_user.profile.name
        dob = current_user.profile.dob
        email = current_user.profile.email
        number = current_user.profile.phone_num

        if request.POST.get('username') != '':
            name = request.POST.get('username')
            current_user.profile.name = name
        if request.POST.get('dob') != '':
            dob = request.POST.get('dob')
            current_user.profile.dob = dob
        if request.POST.get('email') != '':
            email = request.POST.get('email')
            current_user.profile.email = email
        if request.POST.get('number') != '':
            number = request.POST.get('number')
            current_user.profile.phone_num = number
        
        current_user.save()

        print(name)
        return JsonResponse({'name' : name,
                            'dob' : dob,
                            'email' : email,
                            'number' : number})
    return JsonResponse({})

@login_required(login_url='/accounts/login')
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('/profile')

    form = PasswordChangeForm(user=request.user)
    response = {'form': form}
    return render(request, 'profilepage/change_password.html', response)

@login_required(login_url='/accounts/login')
def getProfilelFlutter(request):
    current_user = Profile.objects.filter(user=request.user)
    query = serializers.serialize('json', current_user)
    return HttpResponse(query, content_type="application/json")

@csrf_exempt
def editProfileFlutter(request):
    current_user = request.user
    if request.method == 'POST':
        data = json.loads(request.body)
        print(data)
        name = data['name']
        dob = data['dob']
        email = data['email']
        number = data['number']

        if name != '':
            current_user.profile.name = name
        if dob != '':
            current_user.profile.dob = dob
        if email != '':
            current_user.profile.email = email
        if number != '':
            current_user.profile.phone_num = number
        current_user.save()

        return JsonResponse({'status': 'success'}, status=200)
    else:
        JsonResponse({'status': 'error'}, status=401)
