$(document).ready(function() {
    var formFields = $('.input-wrapper-register');

    formFields.each(function() {
        var field = $(this);
        var input = field.find('input');
        var label = field.find('label');
        
        function checkInput() {
        var valueLength = input.val().length;
        
        if (valueLength > 0 ) {
            label.addClass('focus')
        } else {
                label.removeClass('focus')
        }
        }
        
        input.change(function() {
        checkInput()
        })
    });


    $(".sign-up form").submit(function(e) {
        e.preventDefault();
        $.ajax({
            data: $(this).serialize(),
            type: $(this).attr("method"),
            url: $(this).attr("action"),
            success: function (response) {
                if (response.id == 1){
                    $('.login').addClass('show').removeClass('hide');
                    $('.sign-up').addClass('hide').removeClass('show');                    
                }
                else {
                    let errors = response.message
                    console.log(errors)
                    $(".uname-error").text("")
                    $(".pass-error").text("")

                    for(let x in errors) {
                      if (x === "username"){
                        $(".uname-error").text(errors[x])
                      } else if (x === "password2"){
                        $(".pass-error").text(errors[x])
                        $(".pass-error").css("padding-bottom", "20px")
                      } 
                    }
                }
            },
        })  
    })


})

$(document).ready(function() {
    var formFields = $('.input-wrapper');
  
    formFields.each(function() {
      var field = $(this);
      var input = field.find('input');
      var label = field.find('label');
      var icon = field.find('i');
      
      function checkInput() {
        var valueLength = input.val().length;
        
        if (valueLength > 0 ) {
          label.addClass('focus')
          icon.addClass('focus-icon')
        } else {
              label.removeClass('focus')
              icon.removeClass('focus-icon')
        }
      }
      
      input.change(function() {
        checkInput()
      })
    });
  });