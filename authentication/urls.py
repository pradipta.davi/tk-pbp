from django.urls import path
from .views import signup, login_user, logout_user, handle_signup, login_flutter, signup_flutter, logout_Flutter

app_name = 'authentication'

urlpatterns = [
    path('signup/', signup, name="SignUp"),
    path('signupAjax/', handle_signup, name="SignUpAjax"),
    path('login/', login_user, name="Login"),
    path('loginFlutter', login_flutter, name="LoginFlutter"),
    path('signupFlutter', signup_flutter, name="LoginFlutter"),
    path('logoutFlutter', logout_Flutter, name="LoginFlutter"),
    path('logout/', logout_user, name="Logout"),
]
