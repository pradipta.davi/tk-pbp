from django import forms
from django.contrib.auth.forms import UserCreationForm


ROLE_CHOICES = [
    ('doctor', 'Doctor'),
    ('patient', 'Visitor/Patient'),
]

GENDER_CHOICES = [
    ('male', 'Male'),
    ('female', 'Female')
]


class CreateUserForm(UserCreationForm):
    name = forms.CharField(max_length=100)
    dob = forms.DateField()
    email = forms.EmailField(max_length=100)
    phone_num = forms.CharField(max_length=15)

    # Radio button
    role_choice = forms.CharField(
        label="I'm a", widget=forms.RadioSelect(choices=ROLE_CHOICES))
    gender_choice = forms.CharField(
        label="I'm a", widget=forms.RadioSelect(choices=GENDER_CHOICES))
