from django.contrib.auth.models import User
from django.http.response import JsonResponse
from django.contrib.auth.backends import UserModel  # langsung create model
from django.shortcuts import redirect, render
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
import random
import json


def signup(request):
    context = {}

    form = CreateUserForm(request.POST)

    context['form'] = form
    return render(request, "registerUser.html", context)


def handle_signup(request):
    form = CreateUserForm(request.POST)
    if form.is_valid():
        # Save user
        form.save()

        if request.method == 'POST':

            # Get data from forms.
            username = form.cleaned_data.get('username')
            name = form.cleaned_data.get('name')
            birth_date = form.cleaned_data.get('dob')
            email = form.cleaned_data.get("email")
            gender = form.cleaned_data.get('gender_choice')
            phone_number = form.cleaned_data.get("phone_num")
            role = form.cleaned_data.get("role_choice")

            print(username, name, birth_date,
                  email, gender, phone_number, role)

            # Edit the profile databases.
            update_profile(request, username, name, birth_date,
                           email, phone_number, gender, role)

            return JsonResponse({
                "id": "1",
                "message": "Account created"
            }, safe=False)  # biar ga ngecek jsonnnya valid / ngga

    return JsonResponse({
        "id": "2",
        "message": form.errors
    }, safe=False)


def login_user(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            # redirect ke home
            return redirect('/')

    return render(request, 'login.html')


@csrf_exempt
def login_flutter(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            # Redirect to a success page.
            return JsonResponse({
                "status": True,
                "username": request.user.username,
                "name": user.profile.name,
                "dob": user.profile.dob,
                "email": user.profile.email,
                "phone_num": user.profile.phone_num,
                "role": user.profile.role,
                "gender": user.profile.gender,
                "message": "Successfully Logged In!"
            }, status=200)
        else:
            return JsonResponse({
                "status": False,
                "message": "Failed to Login!"
            }, status=401)

    else:
        return JsonResponse({
            "status": False,
            "message": "Failed to Login, your username/password may be wrong."
        }, status=401)


@csrf_exempt
def signup_flutter(request):
    if request.method == 'POST':
        data = json.loads(request.body)

        username = data["username"]
        email = data["email"]
        password1 = data["password"]
        name = data["name"]
        dob = data["dob"]
        gender = data["gender"]
        phone_num = data["phone_num"]
        role = data["role"]

        if User.objects.filter(username=username):
            print("masuk")
            return JsonResponse({"status": "User Exists"}, status=401)

        newUser = UserModel.objects.create_user(
            username=username,
            password=password1,
        )

        newUser.save()

        update_profile(request, username, name, dob,
                       email, phone_num, gender, role)

        return JsonResponse({"status": "success"}, status=200)
    else:
        return JsonResponse({"status": "error"}, status=401)


@csrf_exempt
def logout_Flutter(request):
    try:
        logout(request)
        return JsonResponse({
            "status": True,
            "message": "Successfully Logged out!"
        }, status=200)
    except:
        return JsonResponse({
            "status": False,
            "message": "Failed to Logout"
        }, status=401)


def logout_user(request):
    logout(request)
    return redirect('/')


def update_profile(request, user_username, name, birth_date, email, phone_number, gender, user_role):
    user = User.objects.get(username=user_username)
    user.profile.name = name
    user.profile.dob = birth_date
    user.profile.email = email
    user.profile.gender = gender
    user.profile.phone_num = phone_number
    user.profile.role = user_role
    user.profile.profil_color = generate_color()

    user.save()


def generate_color():
    COLOR = [
        "#6B46C1",  # ungu
        "#C14646",  # merah
        "#75C146",  # hijau
        "#4665C1",  # biru
        "#c1467b",  # pink
    ]
    return random.choice(COLOR)
